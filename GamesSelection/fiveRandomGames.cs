using System.Linq;
    using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using System;
using System.Xml;

//init script
namespace RutonyChat {
    public class Script {
        private static readonly string MESSAGE_TPL = "[{0}] Игра \"{1}\", была предложена пользователем \"{2}\" {3}";

        /// <summary>
        /// Контекст выполнения скрипта.
        /// Первый файл аргумента param - БД всех игр, второй файл - для сохранения списка 5 рандомных
        /// </summary>
        /// <param name="site">Сайт, с которого скрипт был вызван</param>
        /// <param name="username">Имя пользователя, который вызвал скрипт</param>
        /// <param name="text">Полный текст команды</param>
        /// <param name="param">В данном случае - пути к файлам через точку с запятой</param>
		public void RunScript(string site, string username, string text, string param) {
            string[] fileNames = param.Trim().Split(';');
            string gameDBfileName = fileNames[0].Trim();
            string tempFileName = fileNames[1].Trim();
            if (String.IsNullOrEmpty(gameDBfileName) || String.IsNullOrEmpty(tempFileName)) {
                RutonyBot.BotSay(site, "Параметр должен содержать пути к двум файлам через точку с запятой");
                return;
            }
            if (!File.Exists(gameDBfileName)) {
                RutonyBot.TwitchBot.Say("Список пока пуст");
                return;
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Game>));
            List<Game> games;
            using (Stream stream = File.Open(gameDBfileName, FileMode.Open)) {
                games = (List<Game>) serializer.Deserialize(stream);
            }
            List<Game> selectedGames = new List<Game>();
            int max = games.Count > 5 ? 5 : games.Count;
            Random random = new Random();
            for (int i = 0; i < max; ) {
                int idx = random.Next(games.Count);
                Game game = games[idx];
                if (selectedGames.Contains(game)) continue;
                selectedGames.Add(games[idx]);
                i++;
            }
            
            string message = selectedGames.Select((g, i) => String.Format(MESSAGE_TPL, (i + 1), g.Name, g.Author, g.Date)).Aggregate((current, next) => current + "; " + next);
            RutonyBot.TwitchBot.Say(message);
            
            using (Stream stream = File.Open(tempFileName, FileMode.Create)) {
                serializer.Serialize(stream, selectedGames);
            }
        }
    }

    public class Game {
        [XmlAttribute("name")]
        public string Name {get; set;}

        [XmlAttribute("author")]
        public string Author {get; set;}

        [XmlAttribute("date")]
        public string Date {get; set;}

        public Game(){}

        public Game(string name, string author, string date) {
            Name = name;
            Author = author;
            Date = date;
        }
    }
}