using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using System;
using System.Xml;

//init script
namespace RutonyChat {
    public class Script {
        private static readonly string MESSAGE_TPL = "Игра \"{0}\", была предложена пользователем \"{1}\" {2}";

        /// <summary>
        /// Контекст выполнения скрипта.
        /// Первый файл аргумента param - БД всех игр, второй файл - сохраненный список 5 рандомных, третий файл - исторический лог
        /// </summary>
        /// <param name="site">Сайт, с которого скрипт был вызван</param>
        /// <param name="username">Имя пользователя, который вызвал скрипт</param>
        /// <param name="text">Полный текст команды</param>
        /// <param name="param">В данном случае - пути к файлам через точку с запятой</param>
		public void RunScript(string site, string username, string text, string param) {
            string[] fileNames = param.Trim().Split(';');
            string fileName = fileNames[0].Trim();
            string fiveGamesFileName = fileNames[1].Trim();
            string historyFileName = fileNames[2].Trim();
            if (String.IsNullOrEmpty(fileName) || String.IsNullOrEmpty(historyFileName) || String.IsNullOrEmpty(fiveGamesFileName)) {
                RutonyBot.BotSay(site, "Параметр должен содержать пути к трем файлам через точку с запятой");
                return;
            }
            if (!File.Exists(fileName)) {
                RutonyBot.TwitchBot.Say("Список пока пуст");
                return;
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Game>));
            List<Game> games;
            List<Game> fiveGames;
            using (Stream stream = File.Open(fileName, FileMode.Open)) {
                games = (List<Game>) serializer.Deserialize(stream);
            }
            using (Stream stream = File.Open(fiveGamesFileName, FileMode.Open)) {
                fiveGames = (List<Game>) serializer.Deserialize(stream);
            }

            if (games.Count == 0 || fiveGames.Count == 0) {
                RutonyBot.TwitchBot.Say("Список пока пуст");
                return;
            }

            int idx;
            try {
                idx = int.Parse(text.Split(' ')[1]) - 1;
            } catch (Exception e) {
                if (!(e is ArgumentException) || !(e is ArgumentNullException) || !(e is FormatException)) throw e;
                RutonyBot.TwitchBot.Say("Не удалось распарсить аргумент как целое число");
                return;
            }
            if (idx > 4 || idx < 0) {
                RutonyBot.TwitchBot.Say("Число не может быть больше 5 или меньше 1");
                return;
            }
            Game game = fiveGames[idx];
            string message = String.Format(MESSAGE_TPL, game.Name, game.Author, game.Date);
            RutonyBot.TwitchBot.Say(message);
            games.Remove(games.Find(g => g.Name.Equals(game.Name)));
            using (Stream stream = File.Open(fileName, FileMode.Create)) {
                serializer.Serialize(stream, games);
            }
            using(StreamWriter stream = new StreamWriter(File.Open(historyFileName, FileMode.Append), Encoding.UTF8)) {
                stream.WriteLine("Игра \"{0}\" добавлена {1} {2}, начали играть {3}", 
                    game.Name, game.Author, game.Date, DateTime.Now.ToString("dd.MM.yyyy HH:mm")
                );
            }
        }
    }

    public class Game {
        [XmlAttribute("name")]
        public string Name {get; set;}

        [XmlAttribute("author")]
        public string Author {get; set;}

        [XmlAttribute("date")]
        public string Date {get; set;}

        public Game(){}

        public Game(string name, string author, string date) {
            Name = name;
            Author = author;
            Date = date;
        }
    }
}