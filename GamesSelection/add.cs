using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using System;
using System.Xml;

//init script
namespace RutonyChat {
    public class Script {
        /// <summary>
        /// Контекст выполнения скрипта.
        /// </summary>
        /// <param name="site">Сайт, с которого скрипт был вызван</param>
        /// <param name="username">Имя пользователя, который вызвал скрипт</param>
        /// <param name="text">Полный текст команды</param>
        /// <param name="param">В данном случае - путь к файлу</param>
		public void RunScript(string site, string username, string text, string param) {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Game>));
            string gameName = text.Substring(text.IndexOf(' ')).Trim();
            string fileName = param.Trim();
            if (String.IsNullOrEmpty(fileName)) {
                RutonyBot.BotSay(site, "Параметр должен содержать имя файла");
                return;
            }
            if (String.IsNullOrEmpty(gameName)) { 
                RutonyBot.TwitchBot.Say("Название не может быть пустым!");
                return;
            }
            
            List<Game> games;
            using (Stream stream = File.Open(fileName, FileMode.OpenOrCreate)) {
                games = stream.Length == 0 ? new List<Game>() : (List<Game>) serializer.Deserialize(stream);
            }
            if (games.Find(g => g.Name.Equals(gameName)) != null) {
                RutonyBot.TwitchBot.Say("Эта игра уже есть в списке");
                return;
            }
            games.Add(new Game(gameName, username, DateTime.Now.ToString("dd.MM.yyyy HH:mm")));
            using (Stream stream = File.Open(fileName, FileMode.Create)) {
                serializer.Serialize(stream, games);
            }
            RutonyBot.TwitchBot.Say("Игра успешно добавлена в список");
        }
    }

    public class Game {
        [XmlAttribute("name")]
        public string Name {get; set;}

        [XmlAttribute("author")]
        public string Author {get; set;}

        [XmlAttribute("date")]
        public string Date {get; set;}

        public Game(){}

        public Game(string name, string author, string date) {
            Name = name;
            Author = author;
            Date = date;
        }
   }
}