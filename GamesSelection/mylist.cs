using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System;
using System.Xml;

//init script
namespace RutonyChat {
    
    public class Script {
        private static readonly string MESSAGE_TPL = "Вот все, что ты добавил: ";

        /// <summary>
        /// Контекст выполнения скрипта.
        /// </summary>
        /// <param name="site">Сайт, с которого скрипт был вызван</param>
        /// <param name="username">Имя пользователя, который вызвал скрипт</param>
        /// <param name="text">Полный текст команды</param>
        /// <param name="param">В данном случае - путь к файлу</param>
		public void RunScript(string site, string username, string text, string param) {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Game>));
            string fileName = param.Trim();
            if (String.IsNullOrEmpty(fileName)) {
                RutonyBot.BotSay(site, "Параметр должен содержать имя файла");
                return;
            }
            if (!File.Exists(fileName)) {
                RutonyBot.TwitchBot.Say("Список пока пуст");
                return;
            }
            List<Game> games;
            using (Stream stream = File.Open(fileName, FileMode.Open)) {
                games = (List<Game>) serializer.Deserialize(stream);
            }
            if (games.Count == 0) {
                RutonyBot.TwitchBot.Say("Список пока пуст");
                return;
            }
            string gamesList = games.FindAll(g => g.Author.Equals(username)).Select((g, i) => i + 1 + ". " + g.Name).Aggregate((result, next) => result + ";   " + next);
            RutonyBot.TwitchBot.Say(MESSAGE_TPL + gamesList);
        }
    }

    public class Game {
        [XmlAttribute("name")]
        public string Name {get; set;}

        [XmlAttribute("author")]
        public string Author {get; set;}

        [XmlAttribute("date")]
        public string Date {get; set;}

        public Game(){}

        public Game(string name, string author, string date) {
            Name = name;
            Author = author;
            Date = date;
        }
    }
}