using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System.Collections.Generic;
using System;
using System.Xml;

//init script
namespace RutonyChat {
    public class Script {
        /// <summary>
        /// Контекст выполнения скрипта.
        /// </summary>
        /// <param name="site">Сайт, с которого скрипт был вызван</param>
        /// <param name="username">Имя пользователя, который вызвал скрипт</param>
        /// <param name="text">Полный текст команды</param>
        /// <param name="param">В данном случае - путь к файлу</param>
		public void RunScript(string site, string username, string text, string param) {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Game>));
            string fileName = param.Trim();
            if (String.IsNullOrEmpty(fileName)) {
                RutonyBot.BotSay(site, "Параметр должен содержать имя файла");
                return;
            }
            if (!File.Exists(fileName)) {
                RutonyBot.TwitchBot.Say("Список пока пуст");
                return;
            }
            string idxparam = text.Substring(text.IndexOf(' ')).Trim();
            int idx = 0;
            try {
                idx = int.Parse(idxparam);
            } catch(SystemException e) {
                RutonyBot.TwitchBot.Say("Параметр должен быть числом");
                return;
            }
            if (idx < 1) {
                RutonyBot.TwitchBot.Say("Параметр не может быть меньше 1");
                return;
            }
            
            List<Game> games;
            using (Stream stream = File.Open(fileName, FileMode.OpenOrCreate)) {
                games = stream.Length == 0 ? new List<Game>() : (List<Game>) serializer.Deserialize(stream);
            }
            List<Game> filteredGames = games.FindAll(g => g.Author.Equals(username));
            if (idx > filteredGames.Count) {
                RutonyBot.TwitchBot.Say("Ты столько игр еще не добавил");
                return;
            }

            Game gameToRemove = filteredGames[idx - 1];
            games.Remove(gameToRemove);
            using (Stream stream = File.Open(param, FileMode.Create)) {
                serializer.Serialize(stream, games);
            }
            RutonyBot.TwitchBot.Say("Игра успешно удалена из списка");
        }
    }

    public class Game {
        [XmlAttribute("name")]
        public string Name {get; set;}

        [XmlAttribute("author")]
        public string Author {get; set;}

        [XmlAttribute("date")]
        public string Date {get; set;}

        public Game(){}

        public Game(string name, string author, string date) {
            Name = name;
            Author = author;
            Date = date;
        }

        public override bool Equals(object o) {
            if ((o == null) || !this.GetType().Equals(o.GetType())) return false;
            Game game = (Game) o;
            return this.Name.Equals(game.Name);
        }

        public override int GetHashCode() {
            return this.Name.GetHashCode();
        }
    }
}
