using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using System;
using System.Xml;

//init script
namespace RutonyChat {
    public class Script {
        /// <summary>
        /// Контекст выполнения скрипта.
        /// </summary>
        /// <param name="site">Сайт, с которого скрипт был вызван</param>
        /// <param name="username">Имя пользователя, который вызвал скрипт</param>
        /// <param name="text">Полный текст команды</param>
        /// <param name="param">В данном случае - путь к файлу с цитатами</param>
		public void RunScript(string site, string username, string text, string param) {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Quote>));
            string quote = text.Substring(text.IndexOf(' ')).Trim();
            string fileName = param.Trim();
            if (String.IsNullOrEmpty(fileName)) {
                RutonyBot.BotSay(site, "Параметр должен содержать имя файла");
                return;
            }
            if (String.IsNullOrEmpty(quote)) { 
                RutonyBot.TwitchBot.Say("Текст не может быть пустым!");
                return;
            }
            
            List<Quote> quotes;
            using (Stream stream = File.Open(fileName, FileMode.OpenOrCreate)) {
                quotes = stream.Length == 0 ? new List<Quote>() : (List<Quote>) serializer.Deserialize(stream);
            }
            if (quotes.Find(q => q.Text.Equals(quote)) != null) {
                RutonyBot.TwitchBot.Say("Эта цитата уже есть в списке");
                return;
            }
            quotes.Add(new Quote(quote, username, DateTime.Now.ToString("dd.MM.yyyy HH:mm")));
            using (Stream stream = File.Open(fileName, FileMode.Create)) {
                serializer.Serialize(stream, quotes);
            }
            RutonyBot.TwitchBot.Say("Цитата успешно добавлена в список");
        }
    }

    public class Quote {
        [XmlAttribute("text")]
        public string Text {get; set;}

        [XmlAttribute("author")]
        public string Author {get; set;}

        [XmlAttribute("date")]
        public string Date {get; set;}

        public Quote(){}

        public Quote(string text, string author, string date) {
            Text = text;
            Author = author;
            Date = date;
        }
   }
}