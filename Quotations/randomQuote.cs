using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using System;
using System.Xml;

//init script
namespace RutonyChat {
    public class Script {
        private static readonly string MESSAGE_TPL = "Цитаты великого человека #{0}: {1} ({2})";

        /// <summary>
        /// Контекст выполнения скрипта.
        /// </summary>
        /// <param name="site">Сайт, с которого скрипт был вызван</param>
        /// <param name="username">Имя пользователя, который вызвал скрипт</param>
        /// <param name="text">Полный текст команды</param>
        /// <param name="param">В данном случае - путь к файлу с цитатами</param>
		public void RunScript(string site, string username, string text, string param) {
            string fileName = param.Trim();
            if (String.IsNullOrEmpty(fileName)) {
                RutonyBot.BotSay(site, "Параметр должен содержать путь к файлу");
                return;
            }
            if (!File.Exists(fileName)) {
                RutonyBot.TwitchBot.Say("Список пока пуст");
                return;
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Quote>));
            List<Quote> quotes;
            using (Stream stream = File.Open(fileName, FileMode.Open)) {
                quotes = (List<Quote>) serializer.Deserialize(stream);
            }
            if (quotes.Count == 0) {
                RutonyBot.TwitchBot.Say("Список пока пуст");
                return;
            }
            int idx = (int) Math.Floor(new Random().NextDouble() * quotes.Count);
            Quote quote = quotes[idx];
            string message = String.Format(MESSAGE_TPL, (idx + 1), quote.Text, quote.Date);
            RutonyBot.TwitchBot.Say(message);
        }
    }

    public class Quote {
        [XmlAttribute("text")]
        public string Text {get; set;}

        [XmlAttribute("author")]
        public string Author {get; set;}

        [XmlAttribute("date")]
        public string Date {get; set;}

        public Quote(){}

        public Quote(string text, string author, string date) {
            Text = text;
            Author = author;
            Date = date;
        }
   }
}